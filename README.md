# Quickhull Algorithm Implementation in Python
A python script to draw a convex hull from a list of nodes. Made to fulfill an assignment for Algorithm Strategy course at Institut Teknologi Bandung
## Dependencies
- pyplot
- math (stock)
- random (stock)
- time (stock)