from time import time
from random import uniform

# Randomly generate nodes ahead to accurately measure time
candidate_nodes = list()
n = int(input("How many dots? "))
start = time() # Record the starting time
for i in range(n): candidate_nodes.append((uniform(-100, 100), uniform(-100, 100)))
convex_nodes = list() 

from math import sqrt
from matplotlib import pyplot

def lp_distance(l1, l2, p):
	# A function to calculate the distance between a line (that passes 2 points) with another point 
	# If l1, l2, and p follow the left hand curl with the thumb pointing up, the result will be positive 
	dividend = (l2[1] - l1[1]) * p[0] - (l2[0] - l1[0]) * p[1] + l2[0] * l1[1] - l2[1] * l1[0]
	divisor = sqrt((l2[1] - l1[1]) ** 2 + (l2[0] - l1[0]) ** 2) 
	return dividend / divisor

def move(node):
	# A function to move a node from candidate_nodes to convex_nodes and refresh the graph
	global candidate_nodes, convex_nodes
	# Move the node
	candidate_nodes.remove(node)
	convex_nodes.append(node)
	# Refresh the graph
	if convex_nodes: pyplot.scatter(*zip(*convex_nodes), c='r')
	if candidate_nodes: pyplot.scatter(*zip(*candidate_nodes), c='b')
	pyplot.pause(0.05) # Refresh rate

def make_tent(l1, l2, p):
	# A function that returns the leftline and rightline artist object from 3 points and call the move function
	# l1 and l2 ar the nodes that form the triangle base, following the lp_distance's left hand curl
	leftline_artist, = pyplot.plot(*zip(p, l2), c='r') # "plot returns a list of artists, hence the ," --stackoverflow
	rightline_artist, = pyplot.plot(*zip(l1, p), c='r')
	move(p)
	return leftline_artist, rightline_artist

def triangle_rec(l1, l2, p, leftline_artist, rightline_artist):
	# The quickhull triangle recursion step
	global candidate_nodes, convex_nodes
	# Check if there's node outside the triangle, starting from the left side of the triangle
	farther_nodes = list(filter(lambda candidate_node: lp_distance(p, l2, candidate_node) > 0, candidate_nodes))
	if farther_nodes:
		# Get the node with the max distance from the side of the triangle
		furthest_node = max(farther_nodes, key = lambda farther_node: lp_distance(p, l2, farther_node))	
		# Update line
		leftline_artist.remove()
		leftsubline_artist, rightsubline_artist = make_tent(p, l2, furthest_node)
		# Recursively call the algorithm until there's no node outside the current polygon
		triangle_rec(p, l2, furthest_node, leftsubline_artist, rightsubline_artist)
	# Check the right side of the triangle
	farther_nodes = list(filter(lambda candidate_node: lp_distance(l1, p, candidate_node) > 0, candidate_nodes))
	if farther_nodes:
		furthest_node = max(farther_nodes, key = lambda farther_node: lp_distance(l1, p, farther_node))
		rightline_artist.remove()
		leftsubline_artist, rightsubline_artist = make_tent(l1, p, furthest_node)
		triangle_rec(l1, p, furthest_node, leftsubline_artist, rightsubline_artist)

# Main algorithm
pyplot.ion() # Turn the pyplot interactive mode on so it can update the graph without closing it
# Determine the initial pivot, if candidate_nodes isn't empty
if candidate_nodes:
	xmin_node = min(candidate_nodes, key = lambda node: node[0])
	move(xmin_node)
if candidate_nodes:
	xmax_node = max(candidate_nodes, key = lambda node: node[0])
	move(xmax_node)
# Draw a line on the pivot
pivot_line, = pyplot.plot(*zip(xmin_node, xmax_node), c='r') 
pyplot.pause(0.2)
# Get the topmost node from a list of nodes above the pivot line, if such nodes exist, and begin the recursion
upper_nodes = list(filter(lambda candidate_node: lp_distance(xmax_node, xmin_node, candidate_node) > 0, candidate_nodes))
if upper_nodes:
	top_node = max(upper_nodes, key = lambda upper_node: lp_distance(xmax_node, xmin_node, upper_node))
	leftline_artist, rightline_artist = make_tent(xmax_node, xmin_node, top_node)
	triangle_rec(xmax_node, xmin_node, top_node, leftline_artist, rightline_artist)
# Get the bottommost node from a list of nodes below the pivot line, if such nodes exist, and begin the recursion
lower_nodes = list(filter(lambda candidate_node: lp_distance(xmax_node, xmin_node, candidate_node) < 0, candidate_nodes))
if lower_nodes:
	bottom_node = max(lower_nodes, key = lambda lower_node: lp_distance(xmin_node, xmax_node, lower_node))
	if upper_nodes: pivot_line.remove() # If there are nodes above and below the original pivot, delete the line
	leftline_artist, rightline_artist = make_tent(xmin_node, xmax_node, bottom_node)
	triangle_rec(xmin_node, xmax_node, bottom_node, leftline_artist, rightline_artist)

# Final graph and stats
pyplot.ioff()
print("\nThe nodes that form the convex hull:")
for convex_node in convex_nodes:
	print(convex_node)
print("\nThat took:")
elapsed = time() - start 
print(str(elapsed) + " second(s) (including pyplot's pauses)")
print(str(elapsed - 0.2 - 0.05 * len(convex_nodes)) + " second(s) (without the pauses)")
pyplot.show()